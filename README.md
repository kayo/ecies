# ECIES Cryptography Library

## Overview

   The ECIES implementation based on Phrack Stuff's code from #63 with some changes.

## Differences from original code

   - Data types for public/private keys and stream.
   - Hex bit strings replaced by binary strings.
   - Static initialization using compile-time constants.
   - Encryption/decryption in-place instead of data copy.
   - Some optimizations for embedded platforms.

## Library documentation

   To get documentation you can generate it using doxygen.

## Examples of usage

   For example of usage see `demo.c` and `tool.c`
