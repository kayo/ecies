CFLAGS := -Wall -Wextra

exec := ecc

opt ?= 2

ifneq ($(opt),)
CFLAGS += -O$(opt)
endif

dbg ?= no

ifneq ($(dbg),no)
CFLAGS += -g
endif

src := ecc.c hex.c tool.c
obj := $(src:.c=.o)
asm := $(src:.c=.o)

ecc: $(obj)
	@echo LD $@
	@$(LINK.o) $^ -o $@

%.o: %.c
	@echo CC $^
	@$(CC) $(CFLAGS) -o $@ -c $<

%.s: %.c
	@echo CC $^
	@$(CC) $(CFLAGS) -o $@ -S $<

clean:
	@rm -f *.o *.s $(exec)
	@rm -rf doc

test: $(exec)
	@./$< k 1234567890 > $@.keys && echo "key generation pass" || echo "key generation fail"
	@echo "This secret demo message will be ECIES encrypted" > $@.source
	@./$< e `head -n1 $@.keys` < $@.source > $@.encrypted && echo "encryption pass" || echo "encryption fail"
	@./$< d `tail -n1 $@.keys` < $@.encrypted > $@.decrypted && echo "decription pass" || echo "decryption fail"
	@diff $@.source $@.decrypted && echo "test pass" || echo "test fail"
	@rm -f $@.keys $@.source $@.encrypted $@.decrypted

doc:
	doxygen

.PHONY: doc
